﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageZone : MonoBehaviour
{
   
    void OnTriggerStay2D(Collider2D collider) {

        RubyController rubController = collider.GetComponent<RubyController>();

        if(rubController != null){

            rubController.ChangeHealth(-1);

        }

        
    }

}
